# Recruiting Tools: Web Developer Test
## Digital Recorder
_**Authors:** [@FabioFazio](mailto:fmfazio@gmail.com), [@CristianAlessandrini](cristian.alessandrini@gmail.com)_

This test will involve candidate to produce a very-simple web application.
Basic technologies required are Sql, Php, Html and Javascript.

### How to face this challenge
This exercice **will not care** about:
 - **production time**: time depends on quality candidate wants to grant
   - _"Take your time and show your best!"_
 - **only owned code**
   - _"Opensource libraries and framework are good things!"_
 - **perfection**: there are infinete solutions for a good result
   - _"Creativity is a good ingredient to cook your code"_
   - _"**Simple and Easy** are a good way to walk"_
 
This exercice **will care** about:
 - **Code quality**
   - _Readability, Editability, Inline Documentations, ..._
 - **All candidate skills** he/she wants to show
   - _**Look & Feel, Usability**, Frameworks, libraries, HTML5 Web Design, ..._
 - **Respect requirements** giving client what he/she asked
   - "MORE THEN THIS" will be appreciated, "OTHER THEN THIS" will not

## Basic Requirements
  - Generate a **web page with javascript** (jquery or more is optional) + HTML as follow:
    - [KEYBOARD] A grid of buttons (size min 3x3)
      -  Each button pressed will trigger with a simple animation
    - [STORE] A list of selectable labels
      - Each labels is selectable. Each selection forget previous.
    - Three core buttons in page will lauch main features:
      - [RECORD] : Will start memorize pression sequence in KEYBOARD
      - [STOP & SAVE] : will stop recording KEYBOARD and ask user a label to save current session to STORE
      - [PLAY] : will play sequence from selected session (pression animation in sequence, timed highlights in sequence or as candidate will choose)
  - Prepare two **Back-End services in PHP** (libraries or framework are optionals) following design pattern MVC:
    - [INIT] prepare a schema for a db SQL to support next features (for this test SQLite is recommended)
    - [STOP & SAVE] will store sequence and label for this session on database
    - [PLAY] will return selected sequence to be shown on client page

## Extra Requirements (optional for test)
 - Client isn't sure about grid size. Could be changed on-the-fly?
 - If server is offline (php not available) client would like this app will work OFFLINE: use browser local storage

## Simple Page Concept

| F1 | F2 | F3 |
| - | - | - | 
| 1 | 2 | 3 |
| 4 | 5 | 6 |
| 7 | 8 | 9 |
| * | 0 | # |

[**RECORD**] 
[**STOP & SAVE**]  - Session Name: [ write here ] 

|_Saved Sessions_|
| - |
[ ] PIN Lock Phone |
[ ] PIN Lock SIM |
[ ] CVV Visa Card |
[**PLAY**]
