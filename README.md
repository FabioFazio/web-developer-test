# Web Developer Test

 This test collection can be used free to:
 - select candidates in a recruiting session for a web developer position
 - play a simple exercise to involve all known technologies in a simple web application
   
 Basic skills involved:
 - Sql (or no-SQL databases )
 - Php (or Java, C#, Ruby, Phyton, ... )
 - Javascript
 - Html
 - Human creativity (why not?)

 New tests can be added by community following some rules:
  - Simple enough to be valid for Juniors and Seniors
  - Full and working: simple doesn't mean incomplete or not functional
  - Involve as many competencies possible with Full-Stack Engeneer approach
  - We are not searching for quiz with correct and standard answers
  - With a develop contest can be shown many technical skills: problem solving, coding "style", attention for User Interface, Usability, and much much more.

We are happy if this project can be helpfull for your job or for funny nerd contests.
Any advice or observation will be appreciated.
Thanks in advance for you attention.

_**Authors:** [@FabioFazio](mailto:fmfazio@gmail.com), [@CristianAlessandrini](cristian.alessandrini@gmail.com)_